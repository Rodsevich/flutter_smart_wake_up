import 'dart:math';

import 'package:flutter/material.dart';

class DespertadorAnimado extends StatefulWidget {
  final double rotationDistance;
  final int animationDurationMilliseconds;
  const DespertadorAnimado(
      {Key key, this.rotationDistance, this.animationDurationMilliseconds})
      : super(key: key);

  @override
  _DespertadorAnimadoState createState() => _DespertadorAnimadoState();
}

class _DespertadorAnimadoState extends State<DespertadorAnimado>
    with SingleTickerProviderStateMixin {
  // Tween<double> _rotationTween = Tween(begin: 85, end: 95);
  AnimationController _rotationController;

  @override
  void initState() {
    final halfDistance = widget.rotationDistance;
    _rotationController = AnimationController(
        vsync: this,
        lowerBound: pi * (2 - halfDistance),
        upperBound: pi * (2 + halfDistance));
    _rotationController.repeat(
        period: Duration(milliseconds: widget.animationDurationMilliseconds),
        reverse: true);
    super.initState();
  }

  @override
  void dispose() {
    _rotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _rotationController.view,
        builder: (BuildContext context, Widget child) {
          return Transform.rotate(
              origin: Offset(0, 120),
              angle: _rotationController.value,
              child: child);
        },
        child: Image.asset(
          'assets/images/brain_alarm.png',
        ));
  }
}
