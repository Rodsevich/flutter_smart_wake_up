part of 'sonando_bloc.dart';

@immutable
abstract class SonandoEvent {}

class NumeroApretado extends SonandoEvent {
  final int numero;

  NumeroApretado(this.numero);
}

class BorrarUltimoNumero extends SonandoEvent {}

class BorrarTodo extends SonandoEvent {}

class EsperaTerminada extends SonandoEvent {}

class CorroborarNumero extends SonandoEvent {}
