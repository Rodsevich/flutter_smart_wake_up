part of 'sonando_bloc.dart';

abstract class SonandoState {
  final int numero;
  final String formula;

  SonandoState({@required this.numero, @required this.formula});
}

abstract class CargandoEventSonando {
  Duration espera;
}

class ResolviendoNumero extends SonandoState {
  ResolviendoNumero({int numero, String formula})
      : super(formula: formula, numero: numero);
}

class CargandoFormula extends SonandoState with CargandoEventSonando {
  CargandoFormula({int numero, String formula})
      : super(formula: formula, numero: numero) {
    this.espera = Duration(milliseconds: 500);
  }
}

class NumeroIncorrecto extends SonandoState with CargandoEventSonando {
  NumeroIncorrecto({int numero, String formula})
      : super(formula: formula, numero: numero) {
    this.espera = Duration(milliseconds: 500);
  }
}

class NumeroCorrecto extends SonandoState with CargandoEventSonando {
  NumeroCorrecto({int numero, String formula})
      : super(formula: formula, numero: numero) {
    this.espera = Duration(milliseconds: 500);
  }
}

class TerminandoAlarma extends SonandoState {
  TerminandoAlarma({int numero, String formula})
      : super(formula: formula, numero: numero);
}
