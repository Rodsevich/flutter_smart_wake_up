import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'sonando_event.dart';
part 'sonando_state.dart';

class SonandoBloc extends Bloc<SonandoEvent, SonandoState> {
  final Random _random = Random();
  List<int> factores = [1, 1, 2];
  String formula;
  int numero = 4135, resultado = -1, cantidadResoluciones = 2;
  SonandoBloc() : super(CargandoFormula());

  @override
  Stream<SonandoState> mapEventToState(
    SonandoEvent event,
  ) async* {
    // _regenerar();
    // numero++;
    // yield NumeroCorrecto(formula: formula, numero: numero);
    // return;
    if (event is EsperaTerminada) {
      if (cantidadResoluciones == 0) {
        yield TerminandoAlarma(formula: formula, numero: numero);
      } else if (state is CargandoFormula || state is NumeroCorrecto) {
        _regenerar();
      } else if (state is NumeroIncorrecto) {
        numero = 0;
      }
      yield ResolviendoNumero(formula: formula, numero: numero);
    } else if (event is NumeroApretado) {
      if (numero < 10000) {
        numero *= 10;
        numero += event.numero;
        yield ResolviendoNumero(formula: formula, numero: numero);
      }
    } else if (event is CorroborarNumero) {
      if (numero == resultado) {
        yield NumeroCorrecto(formula: formula, numero: numero);
        cantidadResoluciones--;
      } else {
        yield NumeroIncorrecto(formula: formula, numero: numero);
      }
    } else if (event is BorrarTodo) {
      numero = 0;
      yield ResolviendoNumero(formula: formula, numero: numero);
    } else if (event is BorrarUltimoNumero) {
      numero = (numero / 10).floor();
      yield ResolviendoNumero(formula: formula, numero: numero);
    }
  }

  void _regenerar() {
    numero = 0;
    factores = [_random.nextInt(99), _random.nextInt(30), _random.nextInt(99)];
    resultado = factores[0] * factores[1] + factores[2];
    formula = "${factores[0]} * ${factores[1]} + ${factores[2]}";
  }
}
