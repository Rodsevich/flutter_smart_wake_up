import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:im_animations/im_animations.dart';
import 'package:numeric_keyboard/numeric_keyboard.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_wake_up/src/pages/sonando/sonando_bloc.dart';
import 'package:smart_wake_up/src/widgets/despertador_animado.dart';
import 'package:vibrate/vibrate.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';

class SonandoPage extends StatefulWidget {
  @override
  _SonandoPageState createState() => _SonandoPageState();
}

class _SonandoPageState extends State<SonandoPage> {
  Timer _timerVibrar;
  AudioPlayer _audioPlayer;

  @override
  initState() {
    super.initState();
    _timerVibrar = Timer.periodic(Duration(milliseconds: 1500), _vibrar);
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    _reproducirAlarma();
    //TODO: meter la vibración, el sonido y algo más q me esté olvidando???
  }

  @override
  dispose() {
    _terminarAlarma();
    super.dispose();
  }

  Future _reproducirAlarma() async {
    var prefs = await SharedPreferences.getInstance();
    var path = prefs.getString('ringtone');
    if (path != null) {
      var result =
          await _audioPlayer.play(path, isLocal: true, stayAwake: true);
      if (result == 1) return;
    }
    FlutterRingtonePlayer.playAlarm();
  }

  void _terminarAlarma() {
    _timerVibrar.cancel();
    _audioPlayer.stop();
    FlutterRingtonePlayer.stop();
  }

  void _vibrar(_) {
    Vibrate.vibrate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: WillPopScope(
      onWillPop: () {
        //No dejar q vuelva, para eso hacerlo programáicamente desde el Bloc
        return Future.value(false);
      },
      child: Container(
          decoration: BoxDecoration(
            gradient: RadialGradient(
                center: Alignment(0, -0.45),
                colors: [Colors.blueGrey, Colors.blueGrey[900]]),
            // image: DecorationImage(
            //     image: AssetImage('assets/images/brain_alarm.png'))),
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: 20,
                ),
                DespertadorAnimado(
                  animationDurationMilliseconds: 70,
                  rotationDistance: 0.026,
                ),
                SizedBox(
                  height: 20,
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxHeight: 36, minHeight: 36, maxWidth: 300),
                  child: BlocConsumer<SonandoBloc, SonandoState>(
                    listener: (context, state) {
                      if (state is CargandoEventSonando) {
                        Future.delayed((state as CargandoEventSonando).espera)
                            .then((_) {
                          BlocProvider.of<SonandoBloc>(context)
                              .add(EsperaTerminada());
                        });
                      } else if (state is TerminandoAlarma) {
                        _terminarAlarma();
                        Navigator.pop(context);
                      }
                    },
                    builder: (context, state) {
                      if (state is CargandoFormula) {
                        Future.delayed(state.espera).then((_) {
                          BlocProvider.of<SonandoBloc>(context)
                              .add(EsperaTerminada());
                        });
                        return LinearProgressIndicator();
                      } else {
                        return Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              state.formula + " = ",
                              style:
                                  TextStyle(fontSize: 40, color: Colors.white),
                            ),
                            if (state is NumeroCorrecto)
                              AnimacionNumeroCorrecto(
                                  key: ValueKey(state.numero),
                                  duration: state.espera,
                                  child: _resultadoText(state))
                            else if (state is NumeroIncorrecto)
                              HeartBeat(
                                  beatsPerMinute: 300,
                                  child: _resultadoText(state))
                            else
                              _resultadoText(state)
                          ],
                        );
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                NumericKeyboard(
                  onKeyboardTap: (String text) {
                    BlocProvider.of<SonandoBloc>(context)
                        .add(NumeroApretado(int.parse(text)));
                  },
                  textColor: Colors.white,
                  rightIcon: Icon(Icons.done, color: Colors.white),
                  rightButtonFn: () {
                    BlocProvider.of<SonandoBloc>(context)
                        .add(CorroborarNumero());
                  },
                  leftIcon: Icon(
                    Icons.backspace,
                    color: Colors.white,
                  ),
                  leftButtonFn: () {
                    BlocProvider.of<SonandoBloc>(context)
                        .add(BorrarUltimoNumero());
                    // BlocProvider.of<SonandoBloc>(context).add(BorrarTodo());
                  },
                )
              ],
            ),
          )),
    ));
  }

  Text _resultadoText(SonandoState state) {
    return Text(
      state.numero.toString().padLeft(4, '0'),
      style: TextStyle(
          fontSize: 40,
          color: state is NumeroIncorrecto
              ? Colors.red[300]
              : state is NumeroCorrecto ? Colors.green[200] : Colors.white),
    );
  }
}

class AnimacionNumeroCorrecto extends StatelessWidget {
  final Widget child;
  final Duration duration;
  final Tween _tweenDouble = Tween<double>(begin: 0, end: 1);
  final Matrix4Tween _tweenMatrix = Matrix4Tween(
      begin: Matrix4.identity(),
      end: Matrix4(
        1.2, 0, 0, -0.005 /* skew X */, //
        0, 1.2, 0, 0 /* skew Y */, //
        0, 0, 1, 0 /* skew Z */, //
        30 /* desplazamiento X*/, 0 /* despl Y*/, 0 /*d. Z*/,
        0.8 /* escalado */, //
      ));

  AnimacionNumeroCorrecto({Key key, this.child, this.duration})
      : super(key: key);

  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      duration: duration,
      tween: _tweenDouble,
      child: child,
      curve: Curves.fastOutSlowIn,
      builder: (BuildContext context, double value, Widget child) {
        return Transform(
            transform: _tweenMatrix.transform(value),
            alignment: FractionalOffset.center,
            child: Opacity(
              child: child,
              opacity: 1 - value,
            ));
      },
    );
  }
}
