part of 'home_bloc.dart';

@immutable
abstract class HomeState {
  final TimeOfDay horaAlarma;

  HomeState(this.horaAlarma);
}

class HomeInitial extends HomeState {
  HomeInitial(TimeOfDay horaAlarma) : super(horaAlarma);
}

class CargandoHoraAlarma extends HomeState {
  CargandoHoraAlarma(TimeOfDay horaAlarma) : super(horaAlarma);
}

class EligiendoSonido extends HomeState {
  EligiendoSonido(TimeOfDay horaAlarma) : super(horaAlarma);
}

class GuardandoDatos extends HomeState {
  GuardandoDatos(TimeOfDay horaAlarma) : super(horaAlarma);
}

class SonidoGuardado extends HomeState {
  SonidoGuardado(TimeOfDay horaAlarma) : super(horaAlarma);
}

class SonandoAlarma extends HomeState {
  SonandoAlarma(TimeOfDay horaAlarma) : super(horaAlarma);
}

class ErrorHoraAlarma extends HomeState {
  ErrorHoraAlarma(TimeOfDay horaAlarma) : super(horaAlarma);
}
