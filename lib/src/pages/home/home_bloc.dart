import 'dart:async';

import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_event.dart';
part 'home_state.dart';

void sonarAlarma() async {
  var app = (await AppAvailability.getInstalledApps())
      .singleWhere((m) => m['package_name'] == "com.example.smart_wake_up");
  print(await AppAvailability.isAppEnabled(app['package_name']));
  print(await AppAvailability.checkAvailability(app['package_name']));
  AppAvailability.launchApp("com.example.smart_wake_up");
}

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  TimeOfDay horaAlarma = TimeOfDay(hour: 0, minute: 0);

  HomeBloc() : super(CargandoHoraAlarma(TimeOfDay(hour: 0, minute: 0))) {
    SharedPreferences.getInstance().then((SharedPreferences prefs) {
      var horaDelDia = prefs.getInt('horaDelDia') ?? 0;
      var minutoDelDia = prefs.getInt('minutoDelDia') ?? 0;
      horaAlarma = TimeOfDay(hour: horaDelDia, minute: minutoDelDia);
      emit(HomeInitial(horaAlarma));
      var ahora = DateTime.now();
      if (horaAlarma.hour == ahora.hour && horaAlarma.minute == ahora.minute) {
        emit(SonandoAlarma(horaAlarma));
      }
    });
  }

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is HoraAlarmaDefinida) {
      horaAlarma = event.time;
      yield GuardandoDatos(horaAlarma);
      var ahora = DateTime.now();
      var horaManiana = DateTime(
        ahora.year,
        ahora.month,
        ahora.day,
        horaAlarma.hour,
        horaAlarma.minute,
      );
      if (ahora.isAfter(horaManiana)) {
        horaManiana.add(Duration(days: 1));
      }
      var prefs = await SharedPreferences.getInstance();
      await prefs.setInt('horaDelDia', horaAlarma.hour);
      await prefs.setInt('minutoDelDia', horaAlarma.minute);
      if (await AndroidAlarmManager.periodic(Duration(days: 1), 0, sonarAlarma,
          startAt: horaManiana, rescheduleOnReboot: true, wakeup: true)) {
        yield HomeInitial(horaAlarma);
      } else {
        yield ErrorHoraAlarma(horaAlarma);
      }
    } else if (event is DefinirHoraAlarma) {
      yield CargandoHoraAlarma(horaAlarma);
    } else if (event is SonarAlarma) {
      await Future.delayed(event.delay);
      yield SonandoAlarma(horaAlarma);
      // } else if (event is PararAlarma) {
      //   yield HomeInitial(horaAlarma);
    } else if (event is ElegirSonido) {
      yield EligiendoSonido(horaAlarma);
    } else if (event is SonidoElegido) {
      yield GuardandoDatos(horaAlarma);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (await prefs.setString('ringtone', event.path))
        yield SonidoGuardado(horaAlarma);
    }
  }
}
