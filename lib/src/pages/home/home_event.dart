part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

// class PararAlarma extends HomeEvent {}

class SonarAlarma extends HomeEvent {
  final Duration delay;

  SonarAlarma(this.delay);
}

class DefinirHoraAlarma extends HomeEvent {}

class ElegirSonido extends HomeEvent {}

class SonidoElegido extends HomeEvent {
  final String path;

  SonidoElegido(this.path);
}

class HoraAlarmaDefinida extends HomeEvent {
  final TimeOfDay time;

  HoraAlarmaDefinida(this.time);
}
