import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_wake_up/src/pages/home/home_bloc.dart';
import 'package:file_picker_cross/file_picker_cross.dart';

class HomePage extends StatelessWidget {
  final personNextToMe =
      'That reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighborThat reminds me about the time when I was ten and our neighbor, her name was Mrs. Mable, and she said...';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.yellow[100], Colors.yellow[200]])),
        child: BlocConsumer<HomeBloc, HomeState>(
            listener: (BuildContext context, state) {
          if (state is CargandoHoraAlarma) {
            final proximoMinuto = DateTime.now().add(Duration(minutes: 1));
            showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(proximoMinuto))
                .then((_time) {
              BlocProvider.of<HomeBloc>(context).add(HoraAlarmaDefinida(_time));
            });
          } else if (state is ErrorHoraAlarma) {
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text('No se pudo establecer la alarma'),
            ));
          } else if (state is SonandoAlarma) {
            Navigator.of(context).pushNamed('/sonando');
            // Navigator.push(
            //     context, Route(settings: RouteSettings(name: '/sonando')));
          } else if (state is EligiendoSonido) {
            FilePickerCross.pick(type: FileTypeCross.audio).then((filePicker) {
              if (filePicker?.path != null) {
                BlocProvider.of<HomeBloc>(context)
                    .add(SonidoElegido(filePicker.path));
              }
            });
          } else if (state is SonidoGuardado) {
            Scaffold.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds: 2),
              content: Text('Ringtone exitosamente seleccionado'),
            ));
          }
        }, builder: (BuildContext context, state) {
          const estiloBotones = TextStyle(color: Colors.white, fontSize: 20);
          return SafeArea(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.asset('assets/images/brain_alarm.png'),
                  Row(mainAxisSize: MainAxisSize.min, children: [
                    Text(
                      'Hora: ',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    GestureDetector(
                      onTap: () {
                        BlocProvider.of<HomeBloc>(context)
                            .add(DefinirHoraAlarma());
                      },
                      child: Container(
                        color: Theme.of(context).primaryColorDark,
                        child: Padding(
                          padding:
                              EdgeInsets.symmetric(vertical: 2, horizontal: 6),
                          child: Text(
                            // '12:34',
                            "${state.horaAlarma.hour.toString().padLeft(2, '0')}"
                            ":"
                            "${state.horaAlarma.minute.toString().padLeft(2, '0')}",
                            style: TextStyle(
                                color: Theme.of(context).primaryColorLight,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .fontSize),
                          ),
                        ),
                      ),
                    )
                  ]),
                  if (state is GuardandoDatos) CircularProgressIndicator(),
                  RaisedButton(
                    child: Text('Elegir sonido', style: estiloBotones),
                    color: Colors.deepOrange[600],
                    onPressed: () {
                      BlocProvider.of<HomeBloc>(context).add(ElegirSonido());
                    },
                  ),
                  RaisedButton(
                    child: Text('Probar alarma ahora', style: estiloBotones),
                    color: Colors.red,
                    onPressed: () {
                      BlocProvider.of<HomeBloc>(context)
                          .add(SonarAlarma(Duration(seconds: 0)));
                    },
                  ),
                  RaisedButton(
                    color: Colors.red[500],
                    child: Text('Sonar alarma dentro de 3 segundos',
                        style: estiloBotones),
                    onPressed: () {
                      BlocProvider.of<HomeBloc>(context)
                          .add(SonarAlarma(Duration(seconds: 3)));
                    },
                  ),
                  // RaisedButton(
                  //   color: Colors.green,
                  //   child: Text('Parar alarma'),
                  //   onPressed: () {
                  //     BlocProvider.of<HomeBloc>(context).add(PararAlarma());
                  //   },
                  // ),
                ]),
          );
        }),
      ),
    );
  }
}
