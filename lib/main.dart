import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_wake_up/src/pages/home/home_bloc.dart';
import 'package:smart_wake_up/src/pages/sonando/sonando_bloc.dart';
import 'package:smart_wake_up/src/pages/sonando/ui.dart';
import 'src/pages/home/ui.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AndroidAlarmManager.initialize();
  runApp(App(inicial: '/'));

  // StorageService.initialize().then((value) {
  //   SchedulerService().cleanUp();
  //   AppEvents.setAlarmState(
  //       StorageService.getString('alarmState') ?? 'NO_ALARM');
  // });
}

class App extends StatelessWidget {
  final String inicial;

  const App({Key key, this.inicial}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(create: (BuildContext context) => HomeBloc()),
        BlocProvider<SonandoBloc>(
            create: (BuildContext context) => SonandoBloc()),
      ],
      child: MaterialApp(
        title: 'Smart Wake Up',
        initialRoute: inicial,
        theme: ThemeData(primarySwatch: Colors.orange),
        themeMode: ThemeMode.dark,
        routes: {
          '/': (context) => HomePage(),
          '/sonando': (context) => SonandoPage(),
        },
      ),
    );
  }
}
